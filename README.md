# Symfony bundle to connect host with FZ Hostings

## Installation
Install the bundle with composer
````bash
composer require formatz/fz-hosting-symfony-bundle
````

### If you don't have Symfony Flex
Add the package config to ```config/packages```
````yaml
# config/packages/formatz_fz_hosting.yaml
formatz_fz_hosting:
  api_key: '%env(resolve:FZ_HOSTING_API_KEY)%'
  api_endpoint: '%env(resolve:FZ_HOSTING_API_ENDPOINT)%'
````

Add env variable to ```.env```
````dotenv
FZ_HOSTING_API_ENDPOINT=https://hostings.format-z.ch
FZ_HOSTING_API_KEY=changeme
````

## Send current versions
````bash
php bin/console fz-hosting:send-current-versions
````
