<?php

namespace Formatz\FzHostingBundle\FzHosting;

use Composer\InstalledVersions;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class HostInformationsUpdater
{
    public const API_ENDPOINT_LOG = '/api/v2/log';

    public function __construct(
        private string $apiEndpoint,
        private string $apiKey,
        private HttpClientInterface $httpClient,
        private LoggerInterface $logger,
    ) {
    }

    public function sendCurrentVersions(): ?ResponseInterface
    {
        if (empty($this->apiEndpoint) || empty($this->apiKey)) {
            return null;
        }

        $versions = [
            'php' => PHP_VERSION,
        ];

        $packages = [
            'formatz/fz-hosting-symfony-bundle' => 'fz-hosting-symfony',
            'symfony/framework-bundle' => 'symfony',
            'symfony/console' => 'symfony',
            'contao/core-bundle' => 'contao',
            'isotope/isotope-core' => 'isotope',
            'formatz/contao-headless-cms-bundle' => 'contao-headless-cms',
            'dieschittigs/contao-content-api' => 'contao-headless-cms',
        ];

        foreach ($packages as $packageName => $name) {
            $this->addVersionIfInstalled($versions, $packageName, $name);
        }

        $smtpUrl = $smtpPassword = null;

        if (!empty($_ENV['MAILER_DSN'])) {
            $smtpUrl = urldecode($_ENV['MAILER_DSN']);
            $smtpPassword = parse_url($smtpUrl, PHP_URL_PASS);
        } elseif (!empty($_ENV['MAILER_URL'])) {
            $smtpUrl = urldecode($_ENV['MAILER_URL']);
            $queryString = parse_url($smtpUrl, PHP_URL_QUERY);
            parse_str($queryString, $query);

            // password could exists on query parameter "password".
            // Example : smtp://mail.infomaniak.ch:587?encryption=tls&auth_mode=login&username=smtp@formatlabs.ch&password=***
            $smtpPassword = $query['password'] ?? null;
        }

        $smtp = $smtpUrl ?: '';

        if (!empty($smtpPassword)) {
            $smtp = str_replace($smtpPassword, '***', $smtp);
        }

        $response = $this->httpClient->request(
            'POST',
            $this->apiEndpoint.self::API_ENDPOINT_LOG,
            [
                'headers' => [
                    'FZ-HOSTING-AUTH-TOKEN' => $this->apiKey,
                ],
                'json' => [
                    'versions' => $versions,
                    'smtp' => $smtp,
                ],
            ]
        );

        return $response;
    }

    public function addVersionIfInstalled(array &$versions, string $packageName, string $name): void
    {
        if (InstalledVersions::isInstalled($packageName)) {
            $versions[$name] = InstalledVersions::getVersion($packageName);
        }
    }
}
