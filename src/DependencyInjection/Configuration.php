<?php

namespace Formatz\FzHostingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public const KEY_API_KEY = 'api_key';
    public const KEY_API_ENDPOINT = 'api_endpoint';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('formatz_fz_hosting');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode(self::KEY_API_KEY)->isRequired()->end()
                ->scalarNode(self::KEY_API_ENDPOINT)->isRequired()->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
