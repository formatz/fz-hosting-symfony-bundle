<?php

namespace Formatz\FzHostingBundle\Command;

use Formatz\FzHostingBundle\FzHosting\HostInformationsUpdater;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'fz-hosting:send-current-versions')]
class SendCurrentVersionsCommand extends Command
{
    public function __construct(private HostInformationsUpdater $hostInformationsUpdater, private LoggerInterface $logger)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('ignore-errors', 'i', description: 'Ignore errors');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $ignoreErrors = $input->getOption('ignore-errors');

        $response = $this->hostInformationsUpdater->sendCurrentVersions();

        if (empty($response)) {
            $io->warning($message = 'API endpoint or API key is empty. No version has been sent.');

            if (!$ignoreErrors) {
                $this->logger->warning($message);
            }

            $return = Command::INVALID;
        } else {
            $result = null;

            if (($statusCode = $response->getStatusCode()) >= 300) {
                $result = 401 === $statusCode ? 'Server returned a 401 Unauthorized when sending current packages versions.' : $response->getContent(false);

                if (!$ignoreErrors) {
                    $this->logger->error($result);
                }
            }

            if (is_string($result)) {
                $io->error('An error occured : '.$result);

                $return = Command::FAILURE;
            } else {
                $io->success('Current versions sent !');

                $return = Command::SUCCESS;
            }
        }

        return $ignoreErrors ? Command::SUCCESS : $return;
    }
}
